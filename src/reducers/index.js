import {combineReducers} from 'redux';
import { DEFAULT_STATE, 
        DEFAULT_STATE_FF_EF} from "../utility/constants";

const createReducer = (reducerName, defaultStateParam) => {
    const defaultState = defaultStateParam || DEFAULT_STATE;
    return (state = defaultState, action) => {
        switch(action.type){
            case `${reducerName}_PENDING`:
            case `${reducerName}_FULFILLED`:
            case `${reducerName}_REJECTED`:
                return Object.assign({},action.payload)
            default:
                return state;
        }
    }
}

const rootReducer = combineReducers({
    login: createReducer('LOGIN', DEFAULT_STATE_FF_EF),
    // forgotpassword:createReducer('FORGOT_PASSWORD', DEFAULT_STATE_FF_EF),
    // resetpassword:createReducer('RESET_PASSWORD', DEFAULT_STATE_FF_EF),
    // users: createReducer('USERS'),
    // userAdd: createReducer('USER_ADD', DEFAULT_STATE_FF_EF),
    // userUpdate: createReducer('USER_UPDATE', DEFAULT_STATE_FF_EF),
    // userDelete: createReducer('USER_DELETE', DEFAULT_STATE_FF_EF),
    // userStatus: createReducer('USER_STATUS_UPDATE', DEFAULT_STATE_FF_EF),
    // changePassword: createReducer('CHANGE_PASSWORD', DEFAULT_STATE_FF_EF),
    // user:createReducer('USER', DEFAULT_STATE_FF_EF),
    // image:createReducer('IMAGE_UPLOAD',DEFAULT_STATE_FF_EF),
    // role: createReducer('ROLE', DEFAULT_STATE_FF_EF),
    // roles: createReducer('ROLES'),
    // permission: createReducer('PERMISSION', DEFAULT_STATE_FF_EF),
    // permissions: createReducer('PERMISSIONS', DEFAULT_STATE_FF_EF),
    // userProfile:createReducer('USER_PROFILE', DEFAULT_STATE_FF_EF),
    // updateUserProfile:createReducer('UPDATE_USER_PROFILE', DEFAULT_STATE_FF_EF),
    logout:createReducer('LOGOUT',{logout:false}),
})

export default rootReducer