import React, {Component} from 'react';
import { Link } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Authorization from './utility/authorization';

class Home extends Component {

  logout(){
    Authorization.logout();
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            Home
            <br />
            <Link to="/">App</Link>
            <br />
            <button onClick={() => this.logout()}>Logout</button>
        </header>
      </div>
    );
  }
}

export default Home;
