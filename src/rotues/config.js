import React from "react";
import PublicRoute from "./PublicRoute";
import PrivateRoute from "./PrivateRoute";
import App from "../App";
import Home from "../Home";

/**
 * List of routes for the page
 */
export const ROUTE = {
  public: [
    {
      exact: true,
      path: "/",
      meta: {},
      component: App
    }
  ],
  private: [
    {
      exact: true,
      path: "/home",
      meta: {},
      component: Home
    }
  ]
};

/**
 * Function to set route info
 * @param {routeName} routeName
 */
export const setRoutes = routeName => {
  routeName = routeName || "public";
  const route = ROUTE[routeName];
  return route.map((eachRoute, index) => {
    if (routeName === "private") {
      return <PrivateRoute key={index} {...eachRoute} />;
    } else {
      return <PublicRoute key={index} {...eachRoute} />;
    }
  });
};
