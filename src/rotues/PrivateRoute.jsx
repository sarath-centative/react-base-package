import React from "react";
import { Route, Redirect } from "react-router-dom";
import Authorization from "../utility/authorization";

/**
 * If we have a logged-in user, display the component, otherwise redirect to login page.
 */
const PrivateRoute = ({ component: Component, meta, permission, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        console.log("AUTH");
        console.log(Authorization.getAuthUser);
        console.log(Authorization.isLoggedIn());
        // return <Component {...props} />

        if (Authorization.isLoggedIn()) {
          // Un-comment below code when implement the roles & permission

          // if(permission && !Authorization.isAuthorizedPage(permission)){
          //     Component = Loadable({
          //         loader: () => import ('../error/AccessDenied'),
          //         loading: Loader,
          //     })
          // }

          return <Component {...props} />;
        } else {
          sessionStorage.setItem("proute", JSON.stringify(props.location));
          alert("Please login to continue.");
          return <Redirect to={{ pathname: "/" }} />;
        }
      }}
    />
  );
};
export default PrivateRoute;
