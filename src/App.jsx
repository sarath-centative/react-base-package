import React, {Component} from 'react';
import { Link } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Authorization from './utility/authorization';

class App extends Component{

  login(){
    Authorization.login({name:'xxxx',userId:1});
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <br />
            <Link to="/home">Home</Link>
          <br />
            <button onClick={() => this.login()}>Login</button>
        </header>
      </div>
    );
  }
}

export default App;
